/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.sessionBeans;

import java.util.List;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import tp3.entity.CompteBancaire;

/**
 *
 * @author User
 */
@Stateless
@LocalBean
public class GestionnaireDeCompteBancaire 
{
    @PersistenceContext(unitName = "TP3BanqueKenoldRendel-ejbPU")
    private EntityManager em;
    private ArrayList<CompteBancaire> cb;

    public void persist(Object object) 
    {
        em.persist(object);
    }
    
    

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    public void creerCompte(CompteBancaire c) 
    {
        em.persist(c);
    }
    
    /**
     *
     * @return
     */
    public List<CompteBancaire> getAllComptes() 
    {
        Query q = em.createQuery("SELECT c FROM CompteBancaire c");
        return q.getResultList();
        
    }
    
    public void creerComptesTest() 
    {
        creerCompte(new CompteBancaire("John Lennon", 150_000));
        creerCompte(new CompteBancaire("Paul McCartney", 950_000));
        creerCompte(new CompteBancaire("Ringo Starr", 20_000));
        creerCompte(new CompteBancaire("Georges Harrisson", 100_000));
    }
}
