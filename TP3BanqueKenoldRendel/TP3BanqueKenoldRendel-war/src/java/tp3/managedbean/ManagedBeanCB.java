/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3.managedbean;

import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import tp3.entity.CompteBancaire;
import tp3.sessionBeans.GestionnaireDeCompteBancaire;

/**
 *
 * @author User
 */
@Named(value = "managedBeanCB")
@ViewScoped
public class ManagedBeanCB implements Serializable 
{
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    private long id;
    private CompteBancaire compteBancaire;
    private double montant;
    

    /**
     * Creates a new instance of ManagedBeanCB
     */
    
    public ManagedBeanCB()
    {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    

     
    
     public double getMontant() 
     {
        return montant;
     }

    public void setMontant(double montant) 
    {
        this.montant = montant;
    }
    
    
     public List<CompteBancaire>getCustomers()
     {  
        return gestionnaireDeCompteBancaire.getAllComptes();  
 
    }
     public CompteBancaire getCompte()
     {
         return compteBancaire;
     }
     
     public void debiter()
     {
      gestionnaireDeCompteBancaire.debiterCompte(id, montant);
     }
     
    public void loadComptebancaire() {  
    this.compteBancaire = gestionnaireDeCompteBancaire.chercherCompte(id);  
  }  
    
}
